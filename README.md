This is a fork of Maps (by axet) which is a fork of the Maps.me project (former MapsWithMe).
Maps.me is a great map app for Android but it includes tracking and ads which some users don't like.
Thus, axet tried to remove all those unwanted parts from Maps.me.
Unfortunately, Maps.me doesn't let forks use their map data anymore. Axet stated that he lost interested in the project because of that (see issues of their omim repo).

The goal of this project (for now) is to provide recent maps for the latest version of Maps by axet.
If the app Maps should disappear from fdroid or should not run on recent Android devices anymore,
the goal of this project could be extended to provide an updated version of the Android app.

Although a Desktop and an ios app are also part of this repo, they are not used within this project and could be deleted in the future.

If you are familiar with the Maps.me project, you should carefully read the documentation of this project because the map generation differs. We tried to remove all dependencies to external services and make the generation process easier, so that it can be executed by everyone after checking out this repo. 


# Features (of the Android app by axet, compared to vanilla Maps.me)

Features over upstream repo:

  * Track Recording (record your tracks)
  * Custom Bookmarks path (helps backup your bookmarks with apps like: Syncthing)
  * Removed ads and binaries (peace and freedom)
  * Screenlock option (prevent app stay turned on while in pocket with lockscreen enabled)

Please do not create issues for the Android app at the moment, because app developtment is not part of this project now.  


## Check out the repo

This repository contains submodules. Clone it with `git clone --recursive`. If you forgot,
run `git submodule update --init --recursive`.

We moved most of the submodules back to the main repo to keep a consistent version. The only
exceptions are the boost libs. We might move them to the main repo later to get rid of any submodules.


## Compilation

To compile the project, you would need to initialize private key files. Run
`configure.sh` and press Enter to create empty files, good enough to build the Android debug packages.

For detailed installation instructions and Android building process,
see [INSTALL.md](https://github.com/mapsme/omim/tree/master/docs/INSTALL.md).

TODO: Detailed instructions how to build the map generator will follow.

## Building maps

To create one or many map files, first build the project, then use python module [maps_generator](https://github.com/mapsme/omim/tree/master/tools/python/maps_generator).

TODO: Detailed instructions how to generate maps will follow.

## Map styles

MAPS.ME uses its own binary format for map styles, `drules_proto.bin`, which is compiled from
[MapCSS](https://wiki.openstreetmap.org/wiki/MapCSS) using modified Kothic library.
Feature set in MWM files depends on a compiled style, so make sure to rebuild maps after
releasing a style.

For development, use MAPS.ME Designer app along with its generator tool: these allow
for quick rebuilding of a style and symbols, and for producing a zoom-independent
feature set in MWM files.

See [STYLES.md](https://github.com/mapsme/omim/tree/master/docs/STYLES.md) for the
format description, instructions on building a style and some links.

## Authors and License

The source code that this project is based on is Copyright (C) 2015 My.com B.V. (Mail.Ru Group), published under Apache Public License 2.0,
except third-party libraries. See [NOTICE](https://github.com/mapsme/omim/blob/master/NOTICE)
and [data/copyright.html](http://htmlpreview.github.io/?https://github.com/mapsme/omim/blob/master/data/copyright.html) files for more information.
