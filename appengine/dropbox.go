package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

// dropbox

type GetTemporaryLinkReq struct {
	Path string `json:"path"`
}

type GetTemporaryLinkRes struct {
	Path string `json:"path"`
	Link string `json:"link"`
}

// 1) Register new map at: https://www.dropbox.com/developers/apps
// 2) Generate access token
//
// https://www.dropbox.com/developers/documentation/http/documentation#files-get_temporary_link
//

func GetTemporaryLink(client *http.Client, token DropboxToken, Url string) (string, error) {
	b, _ := json.Marshal(GetTemporaryLinkReq{Url})
	req, _ := http.NewRequest("POST", "https://api.dropboxapi.com/2/files/get_temporary_link", bytes.NewBuffer(b))
	req.Header.Add("Authorization", "Bearer "+string(token))
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	res := new(GetTemporaryLinkRes)
	json.NewDecoder(resp.Body).Decode(res)
	if res.Link == "" || res.Path != "" {
		return "", fmt.Errorf("not found %s %s", Url, res.Path)
	}
	return res.Link, nil
}
