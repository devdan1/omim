package com.mapswithme.maps.geofence;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.mapswithme.maps.LightFramework;
import com.mapswithme.maps.MwmApplication;
import com.mapswithme.maps.location.LocationPermissionNotGrantedException;
import com.mapswithme.util.PermissionsUtils;
import com.mapswithme.util.concurrency.UiThread;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GeofenceRegistryImpl implements GeofenceRegistry
{
  private static final int GEOFENCE_MAX_COUNT = 100;
  private static final int GEOFENCE_TTL_IN_DAYS = 3;
  private static final float PREFERRED_GEOFENCE_RADIUS = 100.0f;

  private static final Logger LOG = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.MISC);
  private static final String TAG = GeofenceRegistryImpl.class.getSimpleName();

  @NonNull
  private final Application mApplication;

  public GeofenceRegistryImpl(@NonNull Application application)
  {
    mApplication = application;
  }

  @Override
  public void registerGeofences(@NonNull GeofenceLocation location) throws LocationPermissionNotGrantedException
  {
    checkThread();
    checkPermission();

    List<GeoFenceFeature> features = LightFramework.getLocalAdsFeatures(
        location.getLat(), location.getLon(), location.getRadiusInMeters(), GEOFENCE_MAX_COUNT);

    LOG.d(TAG, "GeoFenceFeatures = " + Arrays.toString(features.toArray()));
    if (features.isEmpty())
      return;
  }

  @Override
  public void unregisterGeofences() throws LocationPermissionNotGrantedException
  {
    checkThread();
    checkPermission();
  }

  private void onAddSucceeded()
  {
    LOG.d(TAG, "onAddSucceeded");
  }

  private void onAddFailed()
  {
    LOG.d(TAG, "onAddFailed");
  }

  private void onRemoveSucceeded()
  {
    LOG.d(TAG, "onRemoveSucceeded");
  }

  private void onRemoveFailed()
  {
    LOG.d(TAG, "onRemoveFailed");
  }

  private void checkPermission() throws LocationPermissionNotGrantedException
  {
    if (!PermissionsUtils.isLocationGranted(mApplication))
      throw new LocationPermissionNotGrantedException();
  }

  private static void checkThread()
  {
    if (!UiThread.isUiThread())
      throw new IllegalStateException("Must be call from Ui thread");
  }

  @NonNull
  private PendingIntent makeGeofencePendingIntent()
  {
    Intent intent = new Intent(mApplication, GeofenceReceiver.class);
    return PendingIntent.getBroadcast(mApplication, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }

  @NonNull
  public static GeofenceRegistry from(@NonNull Application application)
  {
    MwmApplication app = (MwmApplication) application;
    return app.getGeofenceRegistry();
  }
}
