package com.mapswithme.maps.purchase;

import android.support.annotation.NonNull;

import java.util.List;

public interface PurchaseCallback
{
  void onValidationFinish(boolean success);
}
