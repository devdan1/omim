package com.mapswithme.maps.purchase;

import android.content.Context;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapswithme.maps.PrivateVariables;
import com.mapswithme.maps.PurchaseOperationObservable;
import com.mapswithme.util.Utils;

import java.util.List;

public class PurchaseFactory
{
  private PurchaseFactory()
  {
    // Utility class.
  }

  @NonNull
  public static PurchaseController<PurchaseCallback> createAdsRemovalPurchaseController(
      @NonNull Context context)
  {
    String yearlyProduct = PrivateVariables.adsRemovalYearlyProductId();
    String monthlyProduct = PrivateVariables.adsRemovalMonthlyProductId();
    String weeklyProduct = PrivateVariables.adsRemovalWeeklyProductId();
    String[] productIds = Utils.concatArrays(PrivateVariables.adsRemovalNotUsedList(),
                                             yearlyProduct, monthlyProduct, weeklyProduct);
    return new SubscriptionPurchaseController(new PurchaseValidator<ValidationCallback>() {  public void initialize() {}
      public void destroy() {}
      public void validate(@NonNull String purchaseToken) {}
      public boolean hasActivePurchase() { return false; }
      public void validate(@Nullable String serverId, @NonNull String vendor, @NonNull String purchaseData) {}
      public void addCallback(@NonNull ValidationCallback callback) {}
      public void removeCallback() {}
      public void onSave(Bundle outState){}
      public void onRestore(Bundle inState){}
    }, new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    }, SubscriptionType.ADS_REMOVAL, productIds);
  }

  @NonNull
  public static PurchaseController<PurchaseCallback> createBookmarksSubscriptionPurchaseController(
      @NonNull Context context)
  {
    BillingManager<PlayStoreBillingCallback> billingManager
        = new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
    PurchaseOperationObservable observable = PurchaseOperationObservable.from(context);
    PurchaseValidator<ValidationCallback> validator = new DefaultPurchaseValidator(observable);
    // TODO: replace with bookmark subscription when they are ready.
    String yearlyProduct = PrivateVariables.adsRemovalYearlyProductId();
    String monthlyProduct = PrivateVariables.adsRemovalMonthlyProductId();
    String[] productIds = Utils.concatArrays(PrivateVariables.bookmarksSubscriptionNotUsedList(),
                                             yearlyProduct, monthlyProduct);
    return new SubscriptionPurchaseController(validator, billingManager,
                                              SubscriptionType.BOOKMARKS, productIds);
  }

  @NonNull
  public static PurchaseController<PurchaseCallback> createBookmarkPurchaseController(
      @NonNull Context context, @Nullable String productId, @Nullable String serverId)
  {
    return createAdsRemovalPurchaseController(context);
  }

  @NonNull
  public static PurchaseController<PurchaseCallback> createBookmarkPurchaseController(
      @NonNull Context context)
  {
    return createBookmarkPurchaseController(context, null, null);
  }

  @NonNull
  public static PurchaseController<FailedPurchaseChecker> createFailedBookmarkPurchaseController(
      @NonNull Context context)
  {
    PurchaseOperationObservable observable = PurchaseOperationObservable.from(context);
    PurchaseValidator<ValidationCallback> validator = new DefaultPurchaseValidator(observable);
    return new FailedBookmarkPurchaseController(validator, new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {} });
  }

  @NonNull
  public static BillingManager<PlayStoreBillingCallback> createInAppBillingManager(
      @NonNull Context context)
  {
    return new BillingManager<PlayStoreBillingCallback>() {
      public void initialize(@NonNull Activity context) {}
      public void destroy() {}
      public void launchBillingFlowForProduct(@NonNull String productId) {}
      public boolean isBillingSupported() { return false; }
      public void queryExistingPurchases() {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
  }

  @NonNull
  public static BillingManager<PlayStoreBillingCallback> createSubscriptionBillingManager()
  {
    return new BillingManager<PlayStoreBillingCallback>() { public void initialize(@NonNull Activity context) {};
      public void destroy() {};
      public void launchBillingFlowForProduct(@NonNull String productId) {};
      public boolean isBillingSupported() {  return false; };
      public void queryExistingPurchases() {};
      public void queryProductDetails(@NonNull String... productIds) {}
      public void addCallback(@NonNull PlayStoreBillingCallback callback) {}
      public void queryProductDetails(@NonNull List<String> productIds) {}
      public void consumePurchase(@NonNull String purchaseToken) {}
      public void removeCallback(@NonNull PlayStoreBillingCallback callback) {}
    };
  }
}
